package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações do tipo InfoEmpregador
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoEmpregador implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "inclusao") final InfoEmpregadorInclusao inclusao;

    private @XmlElement(name = "alteracao") final InfoEmpregadorAlteracao alteracao;
    
    private @XmlElement(name = "exclusao") final InfoEmpregadorExclusao exclusao;
    
    public static class Builder {
    	private InfoEmpregadorInclusao inclusao;
    	private InfoEmpregadorAlteracao alteracao;
    	private InfoEmpregadorExclusao exclusao;
    }

    public InfoEmpregador() {
    	this.inclusao = null;
		this.alteracao = null;
		this.exclusao = null;
    }
    
	public InfoEmpregador(final Builder builder) {
		this.inclusao = builder.inclusao;
		this.alteracao = builder.alteracao;
		this.exclusao = builder.exclusao;
	}

	public InfoEmpregadorInclusao getInclusao() {
		return this.inclusao;
	}

	public InfoEmpregadorAlteracao getAlteracao() {
		return this.alteracao;
	}

	public InfoEmpregadorExclusao getExclusao() {
		return this.exclusao;
	}
    
}
