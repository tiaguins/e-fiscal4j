package eprecise.efiscal4j.esocial.enumeration;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Informar se o ente público possui Regime Próprio de Previdência Social.
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public enum IndRPPS {
	S,
	N
}
