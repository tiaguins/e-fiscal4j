package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.type.TIdePeriodo;
import eprecise.efiscal4j.esocial.type.TInfoEmpregador;
import eprecise.efiscal4j.esocial.type.TPeriodoValidade;

/**
 * Informações do infoEmpregador - Tipo Alteração
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoEmpregadorAlteracao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "idePeriodo") final TIdePeriodo idePeriodo;
    
    private @XmlElement(name = "infoCadastro") final TInfoEmpregador infoCadastro;
    
    private @XmlElement(name = "novaValidade") final TPeriodoValidade novaValidade;
    
    public static class Builder {
    	private TIdePeriodo idePeriodo;
    	private TInfoEmpregador infoCadastro;
    	private TPeriodoValidade novaValidade;
    	
    	/**
         * @see IdePeriodo
         *
         * @param idePeriodo
         * @return
         */
        public Builder withIdePeriodo(final TIdePeriodo idePeriodo) {
            this.idePeriodo = idePeriodo;
            return this;
        }
        
        /**
         * @see InfoCadastro
         *
         * @param infoCadastro
         * @return
         */
        public Builder withInfoCadastro(final TInfoEmpregador infoCadastro) {
            this.infoCadastro = infoCadastro;
            return this;
        }
        
        /**
         * @see NovaValidade
         *
         * @param novaValidade
         * @return
         */
        public Builder withNovaValidade(final TPeriodoValidade novaValidade) {
            this.novaValidade = novaValidade;
            return this;
        }
    }

    public InfoEmpregadorAlteracao() {
    	this.idePeriodo = null;
    	this.infoCadastro = null;
    	this.novaValidade = null;
    }
    
	public InfoEmpregadorAlteracao(final Builder builder) {
		this.idePeriodo = builder.idePeriodo;
		this.infoCadastro = builder.infoCadastro;
		this.novaValidade = builder.novaValidade;
	}

	public TIdePeriodo getIdePeriodo() {
		return this.idePeriodo;
	}

	public TInfoEmpregador getInfoCadastro() {
		return this.infoCadastro;
	}

	public TPeriodoValidade getNovaValidade() {
		return this.novaValidade;
	}

}
