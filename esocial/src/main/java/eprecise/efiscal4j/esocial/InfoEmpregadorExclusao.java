package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.type.TIdePeriodo;

/**
 * Informações do infoEmpregador - Tipo Exclusão
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoEmpregadorExclusao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "idePeriodo") final TIdePeriodo idePeriodo;
    
    public static class Builder {
    	private TIdePeriodo idePeriodo;
    	
    	/**
         * @see IdePeriodo
         *
         * @param idePeriodo
         * @return
         */
        public Builder withIdePeriodo(final TIdePeriodo idePeriodo) {
            this.idePeriodo = idePeriodo;
            return this;
        }
    }

    public InfoEmpregadorExclusao() {
    	this.idePeriodo = null;    	
    }
    
	public InfoEmpregadorExclusao(final Builder builder) {
		this.idePeriodo = builder.idePeriodo;		
	}

	public TIdePeriodo getIdePeriodo() {
		return this.idePeriodo;
	}
}
