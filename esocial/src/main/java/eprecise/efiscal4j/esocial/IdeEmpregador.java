package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Tipo IdeEmpregador
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class IdeEmpregador implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "tpInsc") @NotNull final Byte tpInsc;

    private @XmlElement(name = "nrInsc") @Size(min = 0, max = 100) final String nrInsc;

    public static class Builder {
    	private Byte tpInsc;
    	private String nrInsc;
    	
    	/**
         * @see TpInsc
         * @param tpInsc
         * @return
         */
        public Builder withTpInsc(final Byte tpInsc) {
            this.tpInsc = tpInsc;
            return this;
        }
        
        public Builder withNrInsc(final String nrInsc) {
            this.nrInsc = nrInsc;
            return this;
        }
    }

    public IdeEmpregador() {
    	this.tpInsc = null;
		this.nrInsc = null;
    }
    
	public IdeEmpregador(final Builder builder) {
		this.tpInsc = builder.tpInsc;
		this.nrInsc = builder.nrInsc;
	}

	public Byte getTpInsc() {
		return this.tpInsc;
	}

	public String getNrInsc() {
		return this.nrInsc;
	}        
}
