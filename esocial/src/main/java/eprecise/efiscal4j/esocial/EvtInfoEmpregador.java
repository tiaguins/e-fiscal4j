package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import eprecise.efiscal4j.esocial.type.TIdeCadastro;

@XmlRootElement(name = "evtInfoEmpregador")
public class EvtInfoEmpregador implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String XSD = "/eprecise/efiscal4j/esocial/xsd/evtInfoEmpregador.xsd";

	private @XmlElement(name = "ideEvento") @NotNull final TIdeCadastro ideEvento;
	
	private @XmlElement(name = "ideEmpregador") @NotNull final IdeEmpregador ideEmpregador;
    
	private @XmlElement(name = "infoEmpregador") @NotNull final InfoEmpregador infoEmpregador;
	
	private @XmlAttribute(name = "Id") @NotNull final String id;

    private @XmlAttribute(name = "versao") @NotNull final String version;
	
	public static class Builder {
    	private TIdeCadastro ideEvento;
    	private IdeEmpregador ideEmpregador;
    	private InfoEmpregador infoEmpregador;
    	private String id;
    	private String version;
    	
    	/**
         * @see IdeEvento
         *
         * @param ideEvento
         * @return
         */
        public Builder withIdeEvento(final TIdeCadastro ideEvento) {
            this.ideEvento = ideEvento;
            return this;
        }
        
        /**
         * @see IdeEmpregador
         *
         * @param ideEmpregador
         * @return
         */
        public Builder withIdeEmpregador(final IdeEmpregador ideEmpregador) {
            this.ideEmpregador = ideEmpregador;
            return this;
        }
        
        /**
         * @see InfoEmpregador
         *
         * @param infoEmpregador
         * @return
         */
        public Builder withInfoEmpregador(final InfoEmpregador infoEmpregador) {
            this.infoEmpregador = infoEmpregador;
            return this;
        }
    }

	public EvtInfoEmpregador() {
		this.ideEvento = null;
		this.ideEmpregador = null;
		this.infoEmpregador = null;
		this.id = null;
		this.version = null;
	}
	
	public EvtInfoEmpregador(final Builder builder) {
		this.ideEvento = builder.ideEvento;
		this.ideEmpregador = builder.ideEmpregador;
		this.infoEmpregador = builder.infoEmpregador;
		this.id = builder.id;
		this.version = builder.version;
	}

	public TIdeCadastro getIdeEvento() {
		return this.ideEvento;
	}

	public IdeEmpregador getIdeEmpregador() {
		return this.ideEmpregador;
	}

	public InfoEmpregador getInfoEmpregador() {
		return this.infoEmpregador;
	}

	public String getId() {
		return this.id;
	}

	public String getVersion() {
		return this.version;
	}
}
