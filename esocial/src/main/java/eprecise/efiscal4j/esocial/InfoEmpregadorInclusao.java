package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.type.TIdePeriodo;
import eprecise.efiscal4j.esocial.type.TInfoEmpregador;

/**
 * Informações do infoEmpregador - Tipo Inclusão
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoEmpregadorInclusao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "idePeriodo") final TIdePeriodo idePeriodo;

    private @XmlElement(name = "infoCadastro") final TInfoEmpregador infoCadastro;
    
    public static class Builder {
    	private TIdePeriodo idePeriodo;
    	private TInfoEmpregador infoCadastro;
    	
    	/**
         * @see IdePeriodo
         *
         * @param idePeriodo
         * @return
         */
    	public Builder withIdePeriodo(final TIdePeriodo idePeriodo) {
            this.idePeriodo = idePeriodo;
            return this;
        }
    	

    	/**
         * @see InfoCadastro
         *
         * @param infoCadastro
         * @return
         */
    	public Builder withInfoCadastro(final TInfoEmpregador infoCadastro) {
            this.infoCadastro = infoCadastro;
            return this;
        }
    }
    
    public InfoEmpregadorInclusao(){
    	this.idePeriodo = null;
    	this.infoCadastro = null;
    }
    
    public InfoEmpregadorInclusao(final Builder builder){
    	this.idePeriodo = builder.idePeriodo;
    	this.infoCadastro = builder.infoCadastro;
    }

	public TIdePeriodo getIdePeriodo() {
		return this.idePeriodo;
	}

	public TInfoEmpregador getInfoCadastro() {
		return this.infoCadastro;
	}
}
