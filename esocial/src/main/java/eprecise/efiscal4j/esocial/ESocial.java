package eprecise.efiscal4j.esocial;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * ESocial
 *
 * @author Tiago Sousa
 *
 */

@XmlRootElement(name = "eSocial")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "xmlns", "eSocial", "evtInfoEmpregador", "signature" })
public class ESocial implements Serializable {

    private static final long serialVersionUID = 1L;
    
    public static final String XSD = "/eprecise/efiscal4j/esocial/xsd/evtInfoEmpregador.xsd";
    
    private @XmlAttribute(name = "xmlns") @NotNull final String xmlns = "http://www.esocial.gov.br/schema/evt/evtInfoEmpregador/v02_00_00";

    private @XmlElement(name = "evtInfoEmpregador") @NotNull final EvtInfoEmpregador evtInfoEmpregador;
    
    public static class Builder {
    	private EvtInfoEmpregador evtInfoEmpregador;
    	
    	/**
         * @see EvtInfoEmpregador
         *
         * @param EvtInfoEmpregador
         * @return
         */
        public Builder withNFeInfo(final EvtInfoEmpregador evtInfoEmpregador) {
            this.evtInfoEmpregador = evtInfoEmpregador;
            return this;
        }
    }

    public ESocial() {
		this.evtInfoEmpregador = null;
	}
    
	public ESocial(final Builder builder) {
		this.evtInfoEmpregador = builder.evtInfoEmpregador;
	}

	public String getXmlns() {
		return this.xmlns;
	}

	public EvtInfoEmpregador getEvtInfoEmpregador() {
		return this.evtInfoEmpregador;
	}
        
}
