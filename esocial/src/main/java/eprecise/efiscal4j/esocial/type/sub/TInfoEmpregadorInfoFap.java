package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações TInfoEmpregador - Tipo InfoFap
 *
 * @author Felipe Bueno
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorInfoFap implements Serializable {

    private static final long serialVersionUID = 1L;
    
    //Validação incompleta
    private @XmlElement(name = "fap") @NotNull final float fap;

    private @XmlElement(name = "procAdmJudFap") @NotNull final InfoFapProcAdmJudFap procAdmJudFap;
    
    public static class Builder {
    	private float fap;
    	private InfoFapProcAdmJudFap procAdmJudFap;    	

    	/**
         * @see Fap
         *
         * @param fap
         * @return
         */
        public Builder withFap(final float fap) {
            this.fap = fap;
            return this;
        }
        
        /**
         * @see InfoFapProcAdmJudFap
         *
         * @param procAdmJudFap
         * @return
         */
        public Builder withProcAdmJudFap(final InfoFapProcAdmJudFap procAdmJudFap) {
            this.procAdmJudFap = procAdmJudFap;
            return this;
        }
    }

    public TInfoEmpregadorInfoFap() {
		this.fap = 0;
		this.procAdmJudFap = null;
	}
    
	public TInfoEmpregadorInfoFap(final Builder builder) {
		this.fap = builder.fap;
		this.procAdmJudFap = builder.procAdmJudFap;
	}

	public float getFap() {
		return this.fap;
	}

	public InfoFapProcAdmJudFap getProcAdmJudFap() {
		return this.procAdmJudFap;
	}
        
}
