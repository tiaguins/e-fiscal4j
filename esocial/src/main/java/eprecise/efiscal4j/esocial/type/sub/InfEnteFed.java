package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações exclusivas dos Entes Federativos com Regime Próprio de Previdência Social - RPPS
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfEnteFed implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "ufEnteFed") final @Size(max=2) String ufEnteFed;
    
    private @XmlElement(name = "codMunic") final @NotNull Integer codMunic;
    
    private @XmlElement(name = "aliqEnteFed") final @NotNull List<AliqEnteFed> aliqEnteFed;
    
    private @XmlElement(name = "limitesRem") final @NotNull List<LimitesRem> limitesRem;

    public static class Builder {
    	private String ufEnteFed;
    	private Integer codMunic;
    	private List<AliqEnteFed> aliqEnteFed;
    	private List<LimitesRem> limitesRem;
    	
    	/**
         * @see UfEnteFed
         *
         * @param ufEnteFed
         * @return
         */
        public Builder withUfEnteFed(final String ufEnteFed) {
            this.ufEnteFed = ufEnteFed;
            return this;
        }
        
        /**
         * @see CodMunic
         *
         * @param codMunic
         * @return
         */
        public Builder withCodMunic(final Integer codMunic) {
            this.codMunic = codMunic;
            return this;
        }
        
        /**
         * @see AliqEnteFed
         *
         * @param aliqEnteFed
         * @return
         */
        public Builder withAliqEnteFed(final List<AliqEnteFed> aliqEnteFed) {
            this.aliqEnteFed = aliqEnteFed;
            return this;
        }
        
        /**
         * @see LimitesRem
         *
         * @param limitesRem
         * @return
         */
        public Builder withLimitesRem(final List<LimitesRem> limitesRem) {
            this.limitesRem = limitesRem;
            return this;
        }
    }

    public InfEnteFed() {
		this.ufEnteFed = null;
		this.codMunic = 0;
		this.aliqEnteFed = null;
		this.limitesRem = null;
	}
    
	public InfEnteFed(final Builder builder) {
		this.ufEnteFed = builder.ufEnteFed;
		this.codMunic = builder.codMunic;
		this.aliqEnteFed = builder.aliqEnteFed;
		this.limitesRem = builder.limitesRem;
	}

	public String getUfEnteFed() {
		return this.ufEnteFed;
	}

	public Integer getCodMunic() {
		return this.codMunic;
	}

	public List<AliqEnteFed> getAliqEnteFed() {
		return this.aliqEnteFed;
	}

	public List<LimitesRem> getLimitesRem() {
		return this.limitesRem;
	}
    
}
