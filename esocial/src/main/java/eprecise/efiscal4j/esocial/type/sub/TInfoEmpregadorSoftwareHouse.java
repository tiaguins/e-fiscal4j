package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.validator.constraints.Email;

import br.com.caelum.stella.bean.validation.CNPJ;

/**
 * Informações da empresa de Software
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorSoftwareHouse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "cnpjSoftHouse") final @CNPJ @Size(max=14) String cnpjSoftHouse;
    
    private @XmlElement(name = "nmRazao") final @Size(min=1, max=115) String nmRazao;
    
    private @XmlElement(name = "nmCont") final @Size(min=2, max=60) String nmCont;
    
    private @XmlElement(name = "telefone") final @Size(min=1, max=13) String telefone;
    
    private @XmlElement(name = "email") final @Email @Size(max=60) String email;
    
    public static class Builder {
    	private String cnpjSoftHouse;
    	private String nmRazao;
    	private String nmCont;
    	private String telefone;
    	private String email;
    	
    	/**
         * @see CnpjSoftHouse
         *
         * @param cnpjSoftHouse
         * @return
         */
        public Builder withCnpjSoftHouse(final String cnpjSoftHouse) {
            this.cnpjSoftHouse = cnpjSoftHouse;
            return this;
        }
        
        /**
         * @see NmRazao
         *
         * @param nmRazao
         * @return
         */
        public Builder withNmRazao(final String nmRazao) {
            this.nmRazao = nmRazao;
            return this;
        }
        
        /**
         * @see NmCont
         *
         * @param nmCont
         * @return
         */
        public Builder withNmCont(final String nmCont) {
            this.nmCont = nmCont;
            return this;
        }
        
        /**
         * @see Telefone
         *
         * @param telefone
         * @return
         */
        public Builder withTelefone(final String telefone) {
            this.telefone = telefone;
            return this;
        }
        
        /**
         * @see Email
         *
         * @param email
         * @return
         */
        public Builder withEmail(final String email) {
            this.email = email;
            return this;
        }
    }

    public TInfoEmpregadorSoftwareHouse() {
		this.cnpjSoftHouse = null;
		this.nmRazao = null;
		this.nmCont = null;
		this.telefone = null;
		this.email = null;
	}
    
	public TInfoEmpregadorSoftwareHouse(final Builder builder) {
		this.cnpjSoftHouse = builder.cnpjSoftHouse;
		this.nmRazao = builder.nmRazao;
		this.nmCont = builder.nmCont;
		this.telefone = builder.telefone;
		this.email = builder.email;
	}

	public String getCnpjSoftHouse() {
		return this.cnpjSoftHouse;
	}

	public String getNmRazao() {
		return this.nmRazao;
	}

	public String getNmCont() {
		return this.nmCont;
	}

	public String getTelefone() {
		return this.telefone;
	}

	public String getEmail() {
		return this.email;
	}

}
