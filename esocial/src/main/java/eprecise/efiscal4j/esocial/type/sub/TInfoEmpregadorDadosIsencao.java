package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações Complementares - Empresas Isentas - Dados da Isenção
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorDadosIsencao implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "siglaMin") @Size(min = 2, max = 5) final String siglaMin;

    private @XmlElement(name = "nrCertif") @Size(min = 1, max = 40) final String nrCertif;
    
    private @XmlElement(name = "dtEmisCertif") final Date dtEmisCertif;
    
    private @XmlElement(name = "dtVencCertif") final Date dtVencCertif;
    
    private @XmlElement(name = "nrProtRenov") @Size(max = 40) final String nrProtRenov;
    
    private @XmlElement(name = "dtProtRenov") final Date dtProtRenov;
    
    private @XmlElement(name = "dtDou") final Date dtDou;
    
    private @XmlElement(name = "pagDou") @Pattern(regexp = "[\\s]*[0-9]*[1-9]+") @Size(max = 5) final Integer pagDou;
    
    public static class Builder {
    	private String siglaMin;
    	private String nrCertif;
    	private Date dtEmisCertif;
    	private Date dtVencCertif;
    	private String nrProtRenov;
    	private Date dtProtRenov;
    	private Date dtDou;
    	private Integer pagDou;
    	
    	/**
    	 * @see SiglaMin
         *
         * @param siglaMin
         * @return
         */
        public Builder withSiglaMin(final String siglaMin) {
            this.siglaMin = siglaMin;
            return this;
        }
        
        /**
    	 * @see NrCertif
         *
         * @param nrCertif
         * @return
         */
        public Builder withNrCertif(final String nrCertif) {
            this.nrCertif = nrCertif;
            return this;
        }
        
        /**
    	 * @see DtEmisCertif
         *
         * @param dtEmisCertif
         * @return
         */
        public Builder withDtEmisCertif(final Date dtEmisCertif) {
            this.dtEmisCertif = dtEmisCertif;
            return this;
        }
        
        /**
    	 * @see DtVencCertif
         *
         * @param dtVencCertif
         * @return
         */
        public Builder withDtVencCertif(final Date dtVencCertif) {
            this.dtVencCertif = dtVencCertif;
            return this;
        }
        
        /**
    	 * @see NrProtRenov
         *
         * @param nrProtRenov
         * @return
         */
        public Builder withNrProtRenov(final String nrProtRenov) {
            this.nrProtRenov = nrProtRenov;
            return this;
        }
        
        /**
    	 * @see DtProtRenov
         *
         * @param dtProtRenov
         * @return
         */
        public Builder withDtProtRenov(final Date dtProtRenov) {
            this.dtProtRenov = dtProtRenov;
            return this;
        }
        
        /**
    	 * @see DtDou
         *
         * @param dtDou
         * @return
         */
        public Builder withDtDou(final Date dtDou) {
            this.dtDou = dtDou;
            return this;
        }
        
        /**
    	 * @see PagDou
         *
         * @param pagDou
         * @return
         */
        public Builder withPagDou(final Integer pagDou) {
            this.pagDou = pagDou;
            return this;
        }
    }

	public TInfoEmpregadorDadosIsencao(final Builder builder) {
		this.siglaMin = builder.siglaMin;
		this.nrCertif = builder.nrCertif;
		this.dtEmisCertif = builder.dtEmisCertif;
		this.dtVencCertif = builder.dtVencCertif;
		this.nrProtRenov = builder.nrProtRenov;
		this.dtProtRenov = builder.dtProtRenov;
		this.dtDou = builder.dtDou;
		this.pagDou = builder.pagDou;
	}
    
	public TInfoEmpregadorDadosIsencao() {
		this.siglaMin = null;
		this.nrCertif = null;
		this.dtEmisCertif = null;
		this.dtVencCertif = null;
		this.nrProtRenov = null;
		this.dtProtRenov = null;
		this.dtDou = null;
		this.pagDou = null;
	}

	public String getSiglaMin() {
		return this.siglaMin;
	}

	public String getNrCertif() {
		return this.nrCertif;
	}

	public Date getDtEmisCertif() {
		return this.dtEmisCertif;
	}

	public Date getDtVencCertif() {
		return this.dtVencCertif;
	}

	public String getNrProtRenov() {
		return this.nrProtRenov;
	}

	public Date getDtProtRenov() {
		return this.dtProtRenov;
	}

	public Date getDtDou() {
		return this.dtDou;
	}

	public Integer getPagDou() {
		return this.pagDou;
	}
}
