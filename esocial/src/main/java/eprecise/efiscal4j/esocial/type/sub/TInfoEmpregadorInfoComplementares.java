package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.type.SituacaoPF;
import eprecise.efiscal4j.esocial.type.SituacaoPJ;

/**
 * Informações complementares sobre o declarante
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorInfoComplementares implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "situacaoPJ") final @NotNull SituacaoPJ situacaoPJ;
    
    private @XmlElement(name = "situacaoPF") final @NotNull SituacaoPF situacaoPF;

    public static class Builder {
    	private SituacaoPJ situacaoPJ;
    	private SituacaoPF situacaoPF;
    	
    	/**
         * @see SituacaoPJ
         *
         * @param situacaoPJ
         * @return
         */
        public Builder withSituacaoPJ(final SituacaoPJ situacaoPJ) {
            this.situacaoPJ = situacaoPJ;
            return this;
        }
        
        /**
         * @see SituacaoPF
         *
         * @param situacaoPF
         * @return
         */
        public Builder withSituacaoPF(final SituacaoPF situacaoPF) {
            this.situacaoPF = situacaoPF;
            return this;
        }		
    }
    
    public TInfoEmpregadorInfoComplementares() {
		this.situacaoPJ = null;
		this.situacaoPF = null;
	}
    
    public TInfoEmpregadorInfoComplementares(final Builder builder) {
		this.situacaoPJ = builder.situacaoPJ;
		this.situacaoPF = builder.situacaoPF;
	}

	public SituacaoPJ getSituacaoPJ() {
		return this.situacaoPJ;
	}

	public SituacaoPF getSituacaoPF() {
		return this.situacaoPF;
	}
}
