package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações exclusivas de organismos internacionais e outras instituições extraterritoriais
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorInfoOrgInternacional implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "indAcordoIsenMulta") final Byte indAcordoIsenMulta;
    
    public static class Builder {
    	private Byte indAcordoIsenMulta;
    	
    	/**
         * @see IndAcordoIsenMulta
         *
         * @param indAcordoIsenMulta
         * @return
         */
        public Builder withIndAcordoIsenMulta(final Byte indAcordoIsenMulta) {
            this.indAcordoIsenMulta = indAcordoIsenMulta;
            return this;
        }
    }

    public TInfoEmpregadorInfoOrgInternacional() {
		this.indAcordoIsenMulta = null;
	}
    
	public TInfoEmpregadorInfoOrgInternacional(final Builder builder) {
		this.indAcordoIsenMulta = builder.indAcordoIsenMulta;
	}

	public Byte getIndAcordoIsenMulta() {
		return this.indAcordoIsenMulta;
	}
    
}
