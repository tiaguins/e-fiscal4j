package eprecise.efiscal4j.esocial.type;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações Complementares - Pessoa Jurídica
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SituacaoPJ implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "indSitPJ") final @NotNull Byte indSitPJ;
    
    public static class Builder {
    	private Byte indSitPJ;
    	
    	/**
         * @see IndSitPJ
         *
         * @param indSitPJ
         * @return
         */
        public Builder withIndSitPJ(final Byte indSitPJ) {
            this.indSitPJ = indSitPJ;
            return this;
        }
    }

	public SituacaoPJ() {
		this.indSitPJ = null;
	}

	public SituacaoPJ(final Builder builder) {
		this.indSitPJ = builder.indSitPJ;
	}

	public Byte getIndSitPJ() {
		return this.indSitPJ;
	}

}
