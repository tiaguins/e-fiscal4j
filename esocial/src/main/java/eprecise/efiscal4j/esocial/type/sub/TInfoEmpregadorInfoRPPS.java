package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.enumeration.IndRPPS;

/**
 * Registro destinado à informação de órgão público relativa a Regime Próprio de Previdência Social - RPPS
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorInfoRPPS implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "indRPPS") final @Size(min=1,max=1) IndRPPS indRPPS;
    
    private @XmlElement(name = "infEnteFed") final @NotNull InfEnteFed infEnteFed;
    
    public static class Builder {
    	private IndRPPS indRPPS;
    	private InfEnteFed infEnteFed;
    	
    	/**
         * @see IndRPPS
         *
         * @param indRPPS
         * @return
         */
        public Builder withIndRPPS(final IndRPPS indRPPS) {
            this.indRPPS = indRPPS;
            return this;
        }
            	
    	/**
         * @see InfEnteFed
         *
         * @param infEnteFed
         * @return
         */
        public Builder withInfEnteFed(final InfEnteFed infEnteFed) {
            this.infEnteFed = infEnteFed;
            return this;
        }
    }
    
    public TInfoEmpregadorInfoRPPS() {
		this.indRPPS = null;
		this.infEnteFed = null;
	}

	public TInfoEmpregadorInfoRPPS(final Builder builder) {
		this.indRPPS = builder.indRPPS;
		this.infEnteFed = builder.infEnteFed;
	}

	public IndRPPS getIndRPPS() {
		return this.indRPPS;
	}

	public InfEnteFed getInfEnteFed() {
		return this.infEnteFed;
	}
}
