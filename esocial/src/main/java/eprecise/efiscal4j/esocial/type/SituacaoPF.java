package eprecise.efiscal4j.esocial.type;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações Complementares - Pessoa Física
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SituacaoPF implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "indSitPF") final @NotNull Byte indSitPF;
    
    public static class Builder {
    	private Byte indSitPF;
    	
    	/**
         * @see IndSitPF
         *
         * @param indSitPF
         * @return
         */
        public Builder withIndSitPF(final Byte indSitPF) {
            this.indSitPF = indSitPF;
            return this;
        }
    }

	public SituacaoPF() {
		this.indSitPF = null;
	}

	public SituacaoPF(final Builder builder) {
		this.indSitPF = builder.indSitPF;
	}

	public Byte getIndSitPF() {
		return this.indSitPF;
	}
}
