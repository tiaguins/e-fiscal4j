package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações InfoFap - Tipo ProcAdmJudFap
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InfoFapProcAdmJudFap implements Serializable {

    private static final long serialVersionUID = 1L;
    
	private @XmlElement(name = "tpProc") @NotNull final Byte tpProc;
	
	private @XmlElement(name = "nrProc") @NotNull final String nrProc;
	
	public static class Builder {
		private Byte tpProc;
		private String nrProc;
		
		/**
		 * @see TpProc
         *
         * @param tpProc
         * @return
         */
        public Builder withTpProc(final Byte tpProc) {
            this.tpProc = tpProc;
            return this;
        }
        
        /**
		 * @see NrProc
         *
         * @param nrProc
         * @return
         */
        public Builder withNrProc(final String nrProc) {
            this.nrProc = nrProc;
            return this;
        }
	}
	
	public InfoFapProcAdmJudFap(){
		this.tpProc = null;
		this.nrProc = null;
	}
	
	public InfoFapProcAdmJudFap(final Builder builder){
		this.tpProc = builder.tpProc;
		this.nrProc = builder.nrProc;
	}

	public Byte getTpProc() {
		return this.tpProc;
	}

	public String getNrProc() {
		return this.nrProc;
	}
		
}
