package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações dos percentuais das alíquotas do Ente Federativo
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Perc implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "percSeg") final @NotNull float percSeg;
    
    private @XmlElement(name = "percEnte") final @NotNull float percEnte;
    
    private @XmlElement(name = "percSupl") final @NotNull float percSupl;
    
    public static class Builder {
    	private float percSeg;
    	private float percEnte;
    	private float percSupl;
	
		/**
	     * @see PercSeg
	     *
	     * @param percSeg
	     * @return
	     */
	    public Builder withPercSeg(final float percSeg) {
	        this.percSeg = percSeg;
	        return this;
	    }
	    
	    /**
	     * @see PercEnte
	     *
	     * @param percEnte
	     * @return
	     */
	    public Builder withPercEnte(final float percEnte) {
	        this.percEnte = percEnte;
	        return this;
	    }
	    
	    /**
	     * @see PercSupl
	     *
	     * @param percSupl
	     * @return
	     */
	    public Builder withPercSupl(final float percSupl) {
	        this.percSupl = percSupl;
	        return this;
	    }
    }

    public Perc() {
		this.percSeg = 0;
		this.percEnte = 0;
		this.percSupl = 0;
	}
    
	public Perc(final Builder builder) {
		this.percSeg = builder.percSeg;
		this.percEnte = builder.percEnte;
		this.percSupl = builder.percSupl;
	}

	public float getPercSeg() {
		return this.percSeg;
	}

	public float getPercEnte() {
		return this.percEnte;
	}

	public float getPercSupl() {
		return this.percSupl;
	}

}
