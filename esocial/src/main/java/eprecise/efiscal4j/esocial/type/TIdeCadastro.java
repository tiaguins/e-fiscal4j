package eprecise.efiscal4j.esocial.type;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações do tipo TIdeCadastro
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TIdeCadastro implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "tpAmb") final Byte tpAmb;
    
    private @XmlElement(name = "procEmi") final Byte procEmi;
    
    private @XmlElement(name = "verProc") @Size(min = 1, max = 20) final String verProc;

    public static class Builder {
    	private Byte tpAmb;
    	private Byte procEmi;
    	private String verProc;
    	
    	/**
         * @see TpAmb
         *
         * @param tpAmb
         * @return
         */
    	public Builder withTpAmb(final Byte tpAmb) {
            this.tpAmb = tpAmb;
            return this;
        }
    	
    	/**
         * @see ProcEmi
         *
         * @param procEmi
         * @return
         */
    	public Builder withProcEmi(final Byte procEmi) {
            this.procEmi = procEmi;
            return this;
        }
    	
    	/**
         * @see VerProc
         *
         * @param verProc
         * @return
         */
    	public Builder withVerProc(final String verProc) {
            this.verProc = verProc;
            return this;
        }
    }
    
    public TIdeCadastro() {
		this.tpAmb = null;
		this.procEmi = null;
		this.verProc = null;
	}
    
	public TIdeCadastro(final Builder builder) {
		this.tpAmb = builder.tpAmb;
		this.procEmi = builder.procEmi;
		this.verProc = builder.verProc;
	}

	public Byte getTpAmb() {
		return this.tpAmb;
	}

	public Byte getProcEmi() {
		return this.procEmi;
	}

	public String getVerProc() {
		return this.verProc;
	}    
    	
}
