package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações dos limites remuneratórios do Ente Federativo
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class LimitesRem implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "ideSubteto") final @NotNull Byte ideSubteto;
    
    private @XmlElement(name = "valSubteto") final @NotNull float valSubteto;
    
    private @XmlElement(name = "idMaior") final @NotNull Byte idMaior;
    
    public static class Builder {
    	private Byte ideSubteto;
    	private float valSubteto;
    	private Byte idMaior;
    	
    	/**
         * @see IdeSubteto
         *
         * @param ideSubteto
         * @return
         */
        public Builder withIdeSubteto(final Byte ideSubteto) {
            this.ideSubteto = ideSubteto;
            return this;
        }
        
        /**
         * @see ValSubteto
         *
         * @param valSubteto
         * @return
         */
        public Builder withValSubteto(final float valSubteto) {
            this.valSubteto = valSubteto;
            return this;
        }
        
        /**
         * @see IdMaior
         *
         * @param idMaior
         * @return
         */
        public Builder withIdMaior(final Byte idMaior) {
            this.idMaior = idMaior;
            return this;
        }
    }

    public LimitesRem() {
		this.ideSubteto = null;
		this.valSubteto = 0;
		this.idMaior = null;
	}
    
	public LimitesRem(final Builder builder) {
		this.ideSubteto = builder.ideSubteto;
		this.valSubteto = builder.valSubteto;
		this.idMaior = builder.idMaior;
	}

	public Byte getIdeSubteto() {
		return this.ideSubteto;
	}

	public float getValSubteto() {
		return this.valSubteto;
	}

	public Byte getIdMaior() {
		return this.idMaior;
	}

}
