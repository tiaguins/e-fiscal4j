package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.hibernate.validator.constraints.Email;

import br.com.caelum.stella.bean.validation.CPF;

/**
 * Informações de Contato
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TInfoEmpregadorContato implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "nmCtt") @Size(min = 2, max = 60) final String nmCtt;
    
    private @XmlElement(name = "cpfCtt") @CPF final String cpfCtt;
    
    private @XmlElement(name = "foneFixo") @Size(max = 13) final String foneFixo;
    
    private @XmlElement(name = "foneCelular") @Size(max = 13) final String foneCelular;
    
    private @XmlElement(name = "email") @Email @Size(max = 60) final String email;

    public static class Builder {
    	private String nmCtt;
    	private String cpfCtt;
    	private String foneFixo;
    	private String foneCelular;
    	private String email;
    	
    	/**
         * @see NmCtt
         *
         * @param nmCtt
         * @return
         */
        public Builder withNmCtt(final String nmCtt) {
            this.nmCtt = nmCtt;
            return this;
        }
        
        /**
         * @see CpfCtt
         *
         * @param cpfCtt
         * @return
         */
        public Builder withCpfCtt(final String cpfCtt) {
            this.cpfCtt = cpfCtt;
            return this;
        }
        
        /**
         * @see FoneFixo
         *
         * @param foneFixo
         * @return
         */
        public Builder withFoneFixo(final String foneFixo) {
            this.foneFixo = foneFixo;
            return this;
        }
        
        /**
         * @see FoneCelular
         *
         * @param foneCelular
         * @return
         */
        public Builder withFoneCelular(final String foneCelular) {
            this.foneCelular = foneCelular;
            return this;
        }
        
        /**
         * @see Email
         *
         * @param email
         * @return
         */
        public Builder withEmail(final String email) {
            this.email = email;
            return this;
        }
    }
    
    public TInfoEmpregadorContato() {
		this.nmCtt = null;
		this.cpfCtt = null;
		this.foneFixo = null;
		this.foneCelular = null;
		this.email = null;
	}
    
    public TInfoEmpregadorContato(final Builder builder) {
		this.nmCtt = builder.nmCtt;
		this.cpfCtt = builder.cpfCtt;
		this.foneFixo = builder.foneFixo;
		this.foneCelular = builder.foneCelular;
		this.email = builder.email;
	}

	public String getNmCtt() {
		return this.nmCtt;
	}

	public String getCpfCtt() {
		return this.cpfCtt;
	}

	public String getFoneFixo() {
		return this.foneFixo;
	}

	public String getFoneCelular() {
		return this.foneCelular;
	}

	public String getEmail() {
		return this.email;
	}
}
