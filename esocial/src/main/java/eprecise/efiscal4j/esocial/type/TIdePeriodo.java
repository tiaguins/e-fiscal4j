package eprecise.efiscal4j.esocial.type;

import java.io.Serializable;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações do Tipo TIdePeriodo
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class TIdePeriodo implements Serializable {

    private static final long serialVersionUID = 1L;

    private @XmlElement(name = "iniValid") @Size(max=7) final String iniValid;
    
    private @XmlElement(name = "fimValid") @Size(max=7) final String fimValid;
    
    public static class Builder {
    	private String iniValid;
    	private String fimValid;
    	
    	/**
         * @see IniValid
         *
         * @param iniValid
         * @return
         */
        public Builder withIniValid(final String iniValid) {
            this.iniValid = iniValid;
            return this;
        }
        
        /**
         * @see FimValid
         *
         * @param fimValid
         * @return
         */
        public Builder withFimValid(final String fimValid) {
            this.fimValid = fimValid;
            return this;
        }
    }

	public TIdePeriodo() {
		this.iniValid = null;
		this.fimValid = null;
	}

	public TIdePeriodo(final Builder builder) {
		this.iniValid = builder.iniValid;
		this.fimValid = builder.fimValid;
	}

	public String getIniValid() {
		return this.iniValid;
	}

	public String getFimValid() {
		return this.fimValid;
	}
    
}
