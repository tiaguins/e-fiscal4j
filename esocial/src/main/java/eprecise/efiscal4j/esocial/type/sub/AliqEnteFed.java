package eprecise.efiscal4j.esocial.type.sub;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * Informações das alíquotas do Ente Federativo
 *
 * @author Tiago Sousa
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class AliqEnteFed implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "tpPublAlvo") final @NotNull Byte tpPublAlvo;
    
    private @XmlElement(name = "descSegDif") final @Size(max=50) String descSegDif;
    
    private @XmlElement(name = "perc") final @NotNull Perc perc;
    
    public static class Builder {
    	private Byte tpPublAlvo;
    	private String descSegDif;
    	private Perc perc;
    	
    	/**
	     * @see TpPublAlvo
	     *
	     * @param tpPublAlvo
	     * @return
	     */
	    public Builder withTpPublAlvo(final Byte tpPublAlvo) {
	        this.tpPublAlvo = tpPublAlvo;
	        return this;
	    }
	    
	    /**
	     * @see DescSegDif
	     *
	     * @param descSegDif
	     * @return
	     */
	    public Builder withDescSegDif(final String descSegDif) {
	        this.descSegDif = descSegDif;
	        return this;
	    }
	    
	    /**
	     * @see Perc
	     *
	     * @param perc
	     * @return
	     */
	    public Builder withPerc(final Perc perc) {
	        this.perc = perc;
	        return this;
	    }
    }

	public AliqEnteFed() {
		this.tpPublAlvo = null;
		this.descSegDif = null;
		this.perc = null;
	}
    
	public AliqEnteFed(final Builder builder) {
		this.tpPublAlvo = builder.tpPublAlvo;
		this.descSegDif = builder.descSegDif;
		this.perc = builder.perc;
	}

	public Byte getTpPublAlvo() {
		return this.tpPublAlvo;
	}

	public String getDescSegDif() {
		return this.descSegDif;
	}

	public Perc getPerc() {
		return this.perc;
	}    
	
}
