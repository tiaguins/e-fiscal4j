package eprecise.efiscal4j.esocial.type;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlElement;

import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorContato;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorDadosIsencao;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorInfoComplementares;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorInfoFap;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorInfoOrgInternacional;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorInfoRPPS;
import eprecise.efiscal4j.esocial.type.sub.TInfoEmpregadorSoftwareHouse;

public class TInfoEmpregador implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private @XmlElement(name = "nmRazao") @Size(min = 1, max = 115) final String nmRazao;

    private @XmlElement(name = "classTrib") @NotNull @Size(max = 2) final String classTrib;
    
    private @XmlElement(name = "natJurid") @NotNull @Size(max = 4) final String natJurid;
    
    private @XmlElement(name = "indCoop") final Byte indCoop;
    
    private @XmlElement(name = "indConstr") final Byte indConstr;
    
    private @XmlElement(name = "indDesFolha") final Byte indDesFolha;
    
    private @XmlElement(name = "indOptRegEletron") final Byte indOptRegEletron;
    
    private @XmlElement(name = "multTabRubricas") @NotNull @Size(min = 1) final String multTabRubricas;
    
    private @XmlElement(name = "nrSiafi") @NotNull @Size(max = 6) final String nrSiafi;
    
    private @XmlElement(name = "infoFap") @NotNull final TInfoEmpregadorInfoFap infoFap;
    
    private @XmlElement(name = "dadosIsencao") @NotNull final TInfoEmpregadorDadosIsencao dadosIsencao;
    
    private @XmlElement(name = "contato") @NotNull final TInfoEmpregadorContato contato;
    
    private @XmlElement(name = "infoOrgInternacional") @NotNull final TInfoEmpregadorInfoOrgInternacional infoOrgInternacional;
    
    private @XmlElement(name = "softwareHouse") @NotNull final TInfoEmpregadorSoftwareHouse softwareHouse;
    
    private @XmlElement(name = "infoComplementares") @NotNull final TInfoEmpregadorInfoComplementares infoComplementares;
    
    private @XmlElement(name = "infoRPPS") @NotNull final TInfoEmpregadorInfoRPPS infoRPPS;
    
    public static class Builder {
    	private String nmRazao;
    	private String classTrib;
    	private String natJurid;
    	private Byte indCoop;
    	private Byte indConstr;
    	private Byte indDesFolha;
    	private Byte indOptRegEletron;
    	private String multTabRubricas;
    	private String nrSiafi;
    	private TInfoEmpregadorInfoFap infoFap;
    	private TInfoEmpregadorDadosIsencao dadosIsencao;
    	private TInfoEmpregadorContato contato;
    	private TInfoEmpregadorInfoOrgInternacional infoOrgInternacional;
    	private TInfoEmpregadorSoftwareHouse softwareHouse;
    	private TInfoEmpregadorInfoComplementares infoComplementares;
    	private TInfoEmpregadorInfoRPPS infoRPPS;
    	
    	/**
         * @see NmRazao
         *
         * @param nmRazao
         * @return
         */
        public Builder withNmRazao(final String nmRazao) {
            this.nmRazao = nmRazao;
            return this;
        }
        
        /**
         * @see ClassTrib
         *
         * @param classTrib
         * @return
         */
        public Builder withClassTrib(final String classTrib) {
            this.classTrib = classTrib;
            return this;
        }
        
        /**
         * @see NatJurid
         *
         * @param natJurid
         * @return
         */
        public Builder withNatJurid(final String natJurid) {
            this.natJurid = natJurid;
            return this;
        }
        
        /**
         * @see IndCoop
         *
         * @param indCoop
         * @return
         */
        public Builder withIndCoop(final Byte indCoop) {
            this.indCoop = indCoop;
            return this;
        }
        
        /**
         * @see IndConstr
         *
         * @param indConstr
         * @return
         */
        public Builder withIndConstr(final Byte indConstr) {
            this.indConstr = indConstr;
            return this;
        }
        
        /**
         * @see IndDesFolha
         *
         * @param indDesFolha
         * @return
         */
        public Builder withIndDesFolha(final Byte indDesFolha) {
            this.indDesFolha = indDesFolha;
            return this;
        }
        
        /**
        * @see IndOptRegEletron
        *
        * @param indOptRegEletron
        * @return
        */
       public Builder withIndOptRegEletron(final Byte indOptRegEletron) {
           this.indOptRegEletron = indOptRegEletron;
           return this;
       }
       
       /**
        * @see MultTabRubricas
        *
        * @param multTabRubricas
        * @return
        */
       public Builder withMultTabRubricas(final String multTabRubricas) {
           this.multTabRubricas = multTabRubricas;
           return this;
       }
       
       /**
        * @see NrSiafi
        *
        * @param nrSiafi
        * @return
        */
       public Builder withNrSiafi(final String nrSiafi) {
           this.nrSiafi = nrSiafi;
           return this;
       }
       
       /**
        * @see TInfoEmpregadorInfoFap
        *
        * @param infoFap
        * @return
        */
       public Builder withInfoFap(final TInfoEmpregadorInfoFap infoFap) {
           this.infoFap = infoFap;
           return this;
       }
       
       /**
        * @see TInfoEmpregadorDadosIsencao
        *
        * @param dadosIsencao
        * @return
        */
       public Builder withDadosIsencao(final TInfoEmpregadorDadosIsencao dadosIsencao) {
           this.dadosIsencao = dadosIsencao;
           return this;
       }
       
       /**
        * @see TInfoEmpregadorContato
        *
        * @param contato
        * @return
        */
       public Builder withContato(final TInfoEmpregadorContato contato) {
           this.contato = contato;
           return this;
       }
       
       /**
        * @see TInfoEmpregadorInfoOrgInternacional
        *
        * @param infoOrgInternacional
        * @return
        */
       public Builder withInfoOrgInternacional(final TInfoEmpregadorInfoOrgInternacional infoOrgInternacional) {
           this.infoOrgInternacional = infoOrgInternacional;
           return this;
       }              
       
       /**
        * @see TInfoEmpregadorSoftwareHouse
        *
        * @param softwareHouse
        * @return
        */
       public Builder withSoftwareHouse(final TInfoEmpregadorSoftwareHouse softwareHouse) {
           this.softwareHouse = softwareHouse;
           return this;
       } 
       
       /**
        * @see TInfoEmpregadorInfoComplementares
        *
        * @param infoComplementares
        * @return
        */
       public Builder withInfoComplementares(final TInfoEmpregadorInfoComplementares infoComplementares) {
           this.infoComplementares = infoComplementares;
           return this;
       }
       
       /**
        * @see TInfoEmpregadorInfoRPPS
        *
        * @param infoRPPS
        * @return
        */
       public Builder withInfoRPPS(final TInfoEmpregadorInfoRPPS infoRPPS) {
           this.infoRPPS = infoRPPS;
           return this;
       }
    }

	public TInfoEmpregador() {
		this.nmRazao = null;
		this.classTrib = null;
		this.natJurid = null;
		this.indCoop = null;
		this.indConstr = null;
		this.indDesFolha = null;
		this.indOptRegEletron = null;
		this.multTabRubricas = null;
		this.nrSiafi = null;
		this.infoFap = null;
		this.dadosIsencao = null;
		this.contato = null;
		this.infoOrgInternacional = null;
		this.softwareHouse = null;
		this.infoComplementares = null;
		this.infoRPPS = null;
	}

	public TInfoEmpregador(final Builder builder) {
		this.nmRazao = builder.nmRazao;
		this.classTrib = builder.classTrib;
		this.natJurid = builder.natJurid;
		this.indCoop = builder.indCoop;
		this.indConstr = builder.indConstr;
		this.indDesFolha = builder.indDesFolha;
		this.indOptRegEletron = builder.indOptRegEletron;
		this.multTabRubricas = builder.multTabRubricas;
		this.nrSiafi = builder.nrSiafi;
		this.infoFap = builder.infoFap;
		this.dadosIsencao = builder.dadosIsencao;
		this.contato = builder.contato;
		this.infoOrgInternacional = builder.infoOrgInternacional;
		this.softwareHouse = builder.softwareHouse;
		this.infoComplementares = builder.infoComplementares;
		this.infoRPPS = builder.infoRPPS;
	}

	public String getNmRazao() {
		return this.nmRazao;
	}

	public String getClassTrib() {
		return this.classTrib;
	}

	public String getNatJurid() {
		return this.natJurid;
	}

	public Byte getIndCoop() {
		return this.indCoop;
	}

	public Byte getIndConstr() {
		return this.indConstr;
	}

	public Byte getIndDesFolha() {
		return this.indDesFolha;
	}

	public Byte getIndOptRegEletron() {
		return this.indOptRegEletron;
	}

	public String getMultTabRubricas() {
		return this.multTabRubricas;
	}

	public String getNrSiafi() {
		return this.nrSiafi;
	}

	public TInfoEmpregadorInfoFap getInfoFap() {
		return this.infoFap;
	}

	public TInfoEmpregadorDadosIsencao getDadosIsencao() {
		return this.dadosIsencao;
	}

	public TInfoEmpregadorContato getContato() {
		return this.contato;
	}

	public TInfoEmpregadorInfoOrgInternacional getInfoOrgInternacional() {
		return this.infoOrgInternacional;
	}

	public TInfoEmpregadorSoftwareHouse getSoftwareHouse() {
		return this.softwareHouse;
	}

	public TInfoEmpregadorInfoComplementares getInfoComplementares() {
		return this.infoComplementares;
	}

	public TInfoEmpregadorInfoRPPS getInfoRPPS() {
		return this.infoRPPS;
	}
    
}
