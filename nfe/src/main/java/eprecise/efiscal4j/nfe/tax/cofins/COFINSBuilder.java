
package eprecise.efiscal4j.nfe.tax.cofins;

public interface COFINSBuilder {

    COFINS build();
}
